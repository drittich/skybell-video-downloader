# skybell-video-downloader

## Overview

This is a Windows console app for downloading SkyBell saved videos. 

## Configuration

You'll need to rename `App.template.config` to `App.config` and edit the settings. Reference this page for details on obtaining the correct settings: https://github.com/thoukydides/homebridge-skybell/wiki/Protocol-HTTPS