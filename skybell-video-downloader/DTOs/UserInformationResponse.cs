﻿using System;
using skybell_video_downloader.Models;

namespace skybell_video_downloader.DTOs
{
	public class UserInformationResponse
	{
		public string firstName { get; set; }
		public string lastName { get; set; }
		public string resourceId { get; set; }
		public DateTime createdAt { get; set; }
		public DateTime updatedAt { get; set; }
		public string id { get; set; }
		public UserLink[] userLinks { get; set; }
	}
}
