﻿using skybell_video_downloader.Models;

namespace skybell_video_downloader.DTOs
{
	public class ApplicationSubscriptionsResponse
	{
		public EventSubscription devicestatusup { get; set; }
		public EventSubscription devicestatusdown { get; set; }
		public EventSubscription devicesensorapi { get; set; }
		public EventSubscription applicationondemand { get; set; }
		public EventSubscription deviceotahttpsdownload { get; set; }
		public EventSubscription deviceotaverified { get; set; }
		public EventSubscription deviceotaprocess { get; set; }
		public EventSubscription devicesensorbutton { get; set; }
		public EventSubscription devicesensormotion { get; set; }
		public EventSubscription devicesensormotioninitial { get; set; }
		public EventSubscription devicesensorsound { get; set; }
		public EventSubscription deviceupdate { get; set; }
		public EventSubscription devicedestroy { get; set; }
		public EventSubscription activityvideoready { get; set; }
		public EventSubscription applicationbuttonlocklock { get; set; }
		public EventSubscription applicationbuttonlockunlock { get; set; }
		public EventSubscription applicationbuttonlighton { get; set; }
		public EventSubscription applicationbuttonlightoff { get; set; }
		public EventSubscription avatarready { get; set; }
	}
}
