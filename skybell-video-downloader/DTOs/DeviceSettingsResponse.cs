﻿namespace skybell_video_downloader.DTOs
{
	public class DeviceSettingsResponse
	{
		public string ring_tone { get; set; }
		public string do_not_ring { get; set; }
		public string do_not_disturb { get; set; }
		public string digital_doorbell { get; set; }
		public string video_profile { get; set; }
		public string mic_volume { get; set; }
		public string speaker_volume { get; set; }
		public string chime_level { get; set; }
		public string motion_threshold { get; set; }
		public string low_lux_threshold { get; set; }
		public string med_lux_threshold { get; set; }
		public string high_lux_threshold { get; set; }
		public string low_front_led_dac { get; set; }
		public string med_front_led_dac { get; set; }
		public string high_front_led_dac { get; set; }
		public string green_r { get; set; }
		public string green_g { get; set; }
		public string green_b { get; set; }
		public string led_intensity { get; set; }
		public string motion_policy { get; set; }
	}

}
