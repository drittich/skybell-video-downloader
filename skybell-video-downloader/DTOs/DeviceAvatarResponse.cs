﻿using System;

namespace skybell_video_downloader.DTOs
{
	public class DeviceAvatarResponse
	{
		public DateTime createdAt { get; set; }
		public string url { get; set; }
	}

}
