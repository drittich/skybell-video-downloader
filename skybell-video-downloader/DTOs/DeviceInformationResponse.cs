﻿using System;

namespace skybell_video_downloader.DTOs
{
	public class DeviceInformationResponse
	{
		public string serialNo { get; set; }
		public string proxy_port { get; set; }
		public string deviceId { get; set; }
		public string localHostname { get; set; }
		public string firmwareVersion { get; set; }
		public string port { get; set; }
		public string timestamp { get; set; }
		public string address { get; set; }
		public string proxy_address { get; set; }
		public string wifiNoise { get; set; }
		public string wifiBitrate { get; set; }
		public string wifiLinkQuality { get; set; }
		public string wifiSnr { get; set; }
		public string mac { get; set; }
		public string wifiTxPwrEeprom { get; set; }
		public string hardwareRevision { get; set; }
		public string wifiSignalLevel { get; set; }
		public string essid { get; set; }
		public DateTime checkedInAt { get; set; }
		public Status status { get; set; }
	}

	public class Status
	{
		public string wifiLink { get; set; }
	}

}
