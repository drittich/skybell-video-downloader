﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skybell_video_downloader.Models
{
	public class UserLink
	{
		public string user { get; set; }
		public string type { get; set; }
		public string partnerName { get; set; }
		public string accessToken { get; set; }
		public object ttl { get; set; }
		public DateTime createdAt { get; set; }
		public DateTime updatedAt { get; set; }
		public Meta[] meta { get; set; }
		public string id { get; set; }
	}
}
