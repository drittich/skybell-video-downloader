﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skybell_video_downloader.Models
{
	public class Location
	{
		public string lat { get; set; }
		public string lng { get; set; }
	}
}
