﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skybell_video_downloader.Models
{
	public class Device : DeviceBase
	{
		public string user { get; set; }
		public string deviceInviteToken { get; set; }
		public Location location { get; set; }
		public Timezone timeZone { get; set; }
		public Avatar avatar { get; set; }
		public string acl { get; set; }
	}
}
