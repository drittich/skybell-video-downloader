﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skybell_video_downloader.Models
{
	public class Meta
	{
		public string deviceId { get; set; }
		public string structureId { get; set; }
		public string deviceType { get; set; }
	}
}
