﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skybell_video_downloader.Models
{
	public class EventSubscription
	{
		public string eventType { get; set; }
		public string subscription { get; set; }
		public string device { get; set; }
		public string default_rule { get; set; }
		public string rule { get; set; }
	}
}
