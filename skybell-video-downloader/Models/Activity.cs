﻿using System;
using System.Collections.Generic;
using System.IO;

namespace skybell_video_downloader.Models
{
	public class Activity
	{
		public string _id { get; set; }
		public DateTime updatedAt { get; set; }
		public DateTime createdAt { get; set; }
		public string device { get; set; }
		public string callId { get; set; }
		public string _event { get; set; }
		public string state { get; set; }
		public DateTime ttlStartDate { get; set; }
		public string videoState { get; set; }
		public string id { get; set; }
		public string media { get; set; }
		public string mediaSmall { get; set; }


		static string DownloadPath = null;

		public string FilePath
		{
			get
			{
				if (DownloadPath == null)
					DownloadPath = Utility.GetSetting("DownloadPath");

				if (DownloadPath == null)
					throw new KeyNotFoundException("No value found for application config setting [DownloadPath]");

				Directory.CreateDirectory(DownloadPath);

				return Path.Combine(DownloadPath, Path.ChangeExtension($"{createdAt.ToLocalTime().ToString("yyyyMMddHHmmss")}_{id}", ".mp4"));
			}
		}
	}
}
