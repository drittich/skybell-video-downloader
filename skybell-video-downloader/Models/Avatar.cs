﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skybell_video_downloader.Models
{
	public class Avatar
	{
		public string bucket { get; set; }
		public string key { get; set; }
		public DateTime createdAt { get; set; }
		public string url { get; set; }
	}
}
