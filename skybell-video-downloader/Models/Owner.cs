﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skybell_video_downloader.Models
{
	public class Owner
	{
		public string _id { get; set; }
		public string firstName { get; set; }
		public string lastName { get; set; }
		public string resourceId { get; set; }
		public DateTime createdAt { get; set; }
		public DateTime updatedAt { get; set; }
	}
}
