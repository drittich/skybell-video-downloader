﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skybell_video_downloader.Models
{
	public class DeviceBase
	{
		public string uuid { get; set; }
		public string resourceId { get; set; }
		public string name { get; set; }
		public string type { get; set; }
		public string status { get; set; }
		public DateTime createdAt { get; set; }
		public DateTime updatedAt { get; set; }
		public string id { get; set; }
	}
}
