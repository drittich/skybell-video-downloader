﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skybell_video_downloader.Models
{
	public class Subscription
	{
		public string user { get; set; }
		public Owner owner { get; set; }
		public DeviceBase device { get; set; }
		public string acl { get; set; }
		public string id { get; set; }
	}
}
