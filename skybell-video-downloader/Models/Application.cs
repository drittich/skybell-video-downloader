﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skybell_video_downloader.Models
{
	public class Application
	{
		public string appId { get; set; }
		public string currentUser { get; set; }
		public string token { get; set; }
		public string protocol { get; set; }
		public string state { get; set; }
		public DateTime createdAt { get; set; }
		public DateTime updatedAt { get; set; }
		public string endpointArn { get; set; }
		public string id { get; set; }
	}
}
