﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using Newtonsoft.Json;

namespace skybell_video_downloader
{
	class Program
	{
		static HttpClient client = new HttpClient();

		static void Main(string[] args)
		{
			var arguments = CommandLineArguments.Parse(args);

			try
			{
				var skybell = new SkyBell();
				skybell.Login();
				var devices = skybell.GetDevices();
				//Log(JsonConvert.SerializeObject(devices, Formatting.Indented));

				var deviceId = devices.First().id;

				//var deviceInformation = skybell.GetDeviceInformation(deviceId);
				//Log(JsonConvert.SerializeObject(deviceInformation, Formatting.Indented));

				//var deviceSettings = skybell.GetDeviceSettings(deviceId);
				//Log(JsonConvert.SerializeObject(deviceSettings, Formatting.Indented));

				var activityList = skybell.GetActivityList(deviceId);

				if (arguments.Verbose)
					Log(JsonConvert.SerializeObject(activityList, Formatting.Indented));

				//File.WriteAllText(@"c:\tmp\activitylist.json", JsonConvert.SerializeObject(activityList, Formatting.Indented));

				var candidates = activityList.Where(a =>
					a.state == "ready"
					&& a.videoState == "download:ready"
					&& !File.Exists(a.FilePath)
				);
				Log($"Found {candidates.Count()} new videos");

				foreach (var video in candidates.OrderBy(a => a.createdAt))
				{
					DownloadVideo(skybell, deviceId, video);
					Log($"{video.FilePath} downloaded");

					Thread.Sleep(30 * 1000);
				}
			}
			catch (Exception e)
			{
				Log(e.Message);
				if (arguments.Verbose)
					Log(e.ToString());
			}

			if (Debugger.IsAttached)
			{
				Console.WriteLine("\nPress any key...");
				Console.ReadKey();
			}
		}

		static void DownloadVideo(SkyBell skybell, string deviceId, Models.Activity video)
		{
			var activityId = video.id;
			var vidStream = skybell.GetActivityVideo(deviceId, activityId);

			using (var fileStream = File.Create(video.FilePath))
			{
				vidStream.Seek(0, SeekOrigin.Begin);
				vidStream.CopyTo(fileStream);
			}
		}

		public static void Log(string s)
		{
			Console.WriteLine(string.Format("{0}: {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), s ?? ""));
		}
	}
}
