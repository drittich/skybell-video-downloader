﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using skybell_video_downloader.DTOs;
using skybell_video_downloader.Models;

namespace skybell_video_downloader
{
	public class SkyBell
	{
		static HttpClient client = new HttpClient();
		static HttpClient downloadClient = new HttpClient();

		// This identifies a particular mobile device that can receive mobile push notifications. It can be an arbitrary string. 
		// The SkyBell HD app uses a version 5 UUID (RFC 4122). If notifications are being used then this needs to be constant 
		// for a particular device, otherwise any random identifier can be used that is constant during a sessions.
		static string AppId;

		// This is used to distinguish between concurrent calls to the same doorbell. Any random identifier can be used that 
		// is constant during a session.
		static string ClientId;

		// The AccessToken is obtained from the Login request, and is specific to a single AppId and ClientId. This header 
		// may be omitted from the Login request itself.
		string AccessToken
		{
			get; set;
		}

		static string UserName;
		static string Password;

		public SkyBell()
		{
			GetConfig();
			ConfigureHttpClients();
		}

		static void ConfigureHttpClients()
		{
			ConfigureApiHttpClient();
			ConfigureDownloadHttpClient();
		}

		static void GetConfig()
		{
			AppId = Utility.GetSetting("AppId");
			ClientId = Utility.GetSetting("ClientId");
			UserName = Utility.GetSetting("UserName");
			Password = Utility.GetSetting("Password");
		}

		static void ConfigureApiHttpClient()
		{
			client.BaseAddress = new Uri("https://cloud.myskybell.com/");

			client.DefaultRequestHeaders.Add("user-agent", "SkyBell/3.4.1 (iPhone9,2; iOS 11.0; loc=en_US; lang=en-US) com.skybell.doorbell/1");
			client.DefaultRequestHeaders.Add("x-skybell-app-id", AppId);
			client.DefaultRequestHeaders.Add("x-skybell-client-id", ClientId);
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
		}

		static void ConfigureDownloadHttpClient()
		{
			downloadClient.DefaultRequestHeaders.Add("user-agent", "SkyBell/3.4.1 (iPhone9,2; iOS 11.0; loc=en_US; lang=en-US) com.skybell.doorbell/1");
		}

		string GetAccessToken()
		{
			var parms = new
			{
				username = UserName,
				password = Password,
				appId = AppId
				// token = Utility.GetSetting("Token")
			};

			var parmsJson = JsonConvert.SerializeObject(parms);
			var content = new StringContent(parmsJson, Encoding.UTF8, "application/json");

			var response = client.PostAsync("api/v3/login/", content).Result;
			response.EnsureSuccessStatusCode();

			var jsonResponse = response.Content.ReadAsStringAsync().Result;
			var ret = JsonConvert.DeserializeObject<LoginResponse>(jsonResponse);

			return ret.access_token;
		}

		public List<Device> GetDevices()
		{
			var jsonResponse = client.GetStringAsync("api/v3/devices/").Result;
			var ret = JsonConvert.DeserializeObject<List<Device>>(jsonResponse);
			return ret;
		}

		public DeviceInformationResponse GetDeviceInformation(string deviceId)
		{
			var jsonResponse = client.GetStringAsync($"api/v3/devices/{deviceId}/info/").Result;
			var ret = JsonConvert.DeserializeObject<DeviceInformationResponse>(jsonResponse);
			return ret;
		}

		public DeviceSettingsResponse GetDeviceSettings(string deviceId)
		{
			var jsonResponse = client.GetStringAsync($"api/v3/devices/{deviceId}/settings/").Result;
			var ret = JsonConvert.DeserializeObject<DeviceSettingsResponse>(jsonResponse);
			return ret;
		}

		public List<Activity> GetActivityList(string deviceId)
		{
			var jsonResponse = client.GetStringAsync($"api/v3/devices/{deviceId}/activities/").Result;
			var ret = JsonConvert.DeserializeObject<List<Activity>>(jsonResponse);
			return ret;
		}

		public void Login()
		{
			AccessToken = GetAccessToken();

			if (client.DefaultRequestHeaders.Contains("authorization"))
				client.DefaultRequestHeaders.Remove("authorization");

			client.DefaultRequestHeaders.Add("authorization", "Bearer " + AccessToken);
		}

		public ActivityVideo GetActivityUrl(string deviceId, string activityId)
		{
			var jsonResponse = client.GetStringAsync($"api/v3/devices/{deviceId}/activities/{activityId}/video/").Result;
			var ret = JsonConvert.DeserializeObject<ActivityVideo>(jsonResponse);
			return ret;
		}

		public Stream GetActivityVideo(string deviceId, string activityId)
		{
			var url = GetActivityUrl(deviceId, activityId).url;

			var response = downloadClient.GetAsync(url).Result;
			response.EnsureSuccessStatusCode();
			var content = response.Content;
			var contentStream = content.ReadAsStreamAsync().Result; // get the actual content stream

			return contentStream;
		}
	}
}
