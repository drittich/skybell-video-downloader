﻿using System;
using DotArguments;
using DotArguments.Attributes;

namespace skybell_video_downloader
{
	public class CommandLineArguments
	{
		//[PositionalValueArgument(0, "inputpath")]
		//[ArgumentDescription(Short = "the input path")]
		//public string InputPath { get; set; }

		//[PositionalValueArgument(1, "outputpath", IsOptional = true)]
		//[ArgumentDescription(Short = "the output path")]
		//public string OutputPath { get; set; }

		//[NamedValueArgument("name", 'n', IsOptional = true)]
		//[ArgumentDescription(Short = "the name")]
		//public string Name { get; set; }

		//[NamedValueArgument("age", IsOptional = true)]
		//[ArgumentDescription(Short = "the age")]
		//public int? Age { get; set; }

		[NamedSwitchArgument("verbose", 'v')]
		[ArgumentDescription(Short = "enable verbose console output")]
		public bool Verbose { get; set; }

		[RemainingArguments]
		public string[] RemainingArguments { get; set; }

		public static CommandLineArguments Parse(string[] args)
		{
			// create container definition and the parser
			ArgumentDefinition definition = new ArgumentDefinition(typeof(CommandLineArguments));
			GNUArgumentParser parser = new GNUArgumentParser();

			CommandLineArguments arguments = null;
			try
			{
				// create object with the populated arguments
				arguments = parser.Parse<CommandLineArguments>(definition, args);
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine(string.Format("error: {0}", ex.Message));
				Console.Error.Write(string.Format("usage: {0}", parser.GenerateUsageString(definition)));

				Environment.Exit(1);
			}

			return arguments;
		}
	}
}
