﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace skybell_video_downloader
{
	public class Utility
	{
		public static string GetSetting(string key, string defaultValue = null)
		{
			string value = System.Configuration.ConfigurationManager.AppSettings[key];
			return value ?? defaultValue;
		}
	}
}
